import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiservice/api-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any;
  categoriesArr = ['all-books', 'bestsellers', 'fiction', 'non-fiction', 'action-adventure'];
  books: any;
  allBooks: any;
  constructor(public navCtrl: NavController, public apiserives: ApiServiceProvider) {

  }

  // Initialize slider
  ionViewDidEnter() {
    this.slideChanged();
    this.getBooks('all-books');
  }
  // On segment click
  selectedTab(index, category) {
    console.log("selectedTab", index)
    this.getBooks(category);
  }


  // On slide changed
  slideChanged() {

  }
  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.books = this.allBooks.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    } else {
      this.books = this.allBooks;
    }
  }

  getBooks(apiname) {
    this.apiserives.loadapiservices(apiname)
      .then(data => {
        this.books = data;
        this.allBooks = data;
        console.log(data);
      });
  }

}
